# gadjian

[Demo](http://gadjian-ram.surge.sh/)

## Pastikan Laptop/PC terkoneksi dengan Internet karena aplikasi ini menggunakan bootstrap 4 melalui CDN. 
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
