export default {
    data() {
        return {
            isEdit: false,
            pilih: '',
            jumlah: '',
            judul: '',
            type: [
                {
                    id: 1,
                    name: 'Pemasukkan',
                },
                {
                    id: 2,
                    name: 'Pengeluaran',
                },
            ],
            tabungan: [{}],
            pemasukkan: [],
            tot_pemasukkan: 0,
            pengeluaran: [],
            tot_pengeluaran: 0,
            tot_uang: 0,
            formToShow: -1,
            newpilih: '',
            newjumlah: '',
            newjudul: '',
            submitForm:false
        }
    },
    mounted() {
        this.tabungan.pop()
    },
    computed: {
        emptySelect() {
            return this.pilih == ''
        },
        emptyJumlah(){
            return this.jumlah == ''
        },
        emptyJudul(){
            return this.judul == ''
        }
    },
    methods: {
        kirim(e) {
            this.submitForm = true
            if (this.emptySelect || this.emptyJumlah || this.emptyJudul) {
                e.preventDefault()
            }
            else {
                this.tabungan.push({
                    tipe: this.pilih,
                    jumlah: this.jumlah,
                    judul: this.judul,
                })

                for (let i = 0; i < this.tabungan.length; i++) {
                    if (this.tabungan[i].tipe == "Pemasukkan") {
                        this.pemasukkan.push(Number(this.tabungan[i].jumlah))
                        this.tabungan[i].newpilih = this.tabungan[i].tipe
                        this.tabungan[i].newjumlah = this.tabungan[i].jumlah
                        this.tabungan[i].newjudul = this.tabungan[i].judul
                    } else {
                        this.pengeluaran.push(Number(this.tabungan[i].jumlah))
                    }

                }

                this.tot_pemasukkan = this.pemasukkan.reduce((a, b) => a + b, 0)
                this.tot_pengeluaran = this.pengeluaran.reduce((a, b) => a + b, 0)

                // reset

                this.pemasukkan = []
                this.pengeluaran = []

                this.pilih = ''
                this.jumlah = ''
                this.judul = ''
            }
        },
        simpan(idx) {
            console.log(this.tabungan[idx].jumlah)
            this.tabungan.splice(idx, 0, this.tabungan[idx].jumlah)
            this.tabungan.splice(idx, 1)
            this.formToShow = -1

            for (let i = 0; i < this.tabungan.length; i++) {
                if (this.tabungan[i].tipe == "Pemasukkan") {
                    this.pemasukkan.push(Number(this.tabungan[i].jumlah))
                } else {
                    this.pengeluaran.push(Number(this.tabungan[i].jumlah))
                }

            }
            this.tot_pemasukkan = this.pemasukkan.reduce((a, b) => a + b, 0)
            this.tot_pengeluaran = this.pengeluaran.reduce((a, b) => a + b, 0)

            this.pemasukkan = []
            this.pengeluaran = []
        },
        hapus(idx) {
            this.tabungan.splice(idx, 1)
            for (let i = 0; i < this.tabungan.length; i++) {
                if (this.tabungan[i].tipe == "Pemasukkan") {
                    this.pemasukkan.push(Number(this.tabungan[i].jumlah))
                } else {
                    this.pengeluaran.push(Number(this.tabungan[i].jumlah))
                }

            }
            this.tot_pemasukkan = this.pemasukkan.reduce((a, b) => a + b, 0)
            this.tot_pengeluaran = this.pengeluaran.reduce((a, b) => a + b, 0)

            this.pemasukkan = []
            this.pengeluaran = []
        }
    }


}